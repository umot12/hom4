package com.example.bootdata.domain.dto;

import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Range;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class CustomerRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    @NotNull
    @Size(min = 2, message = "user name should have at least 2 characters")
    private String name;
    @Email
    private String email;

    private String password;
    @Pattern(regexp = "^((\\+?380)([0-9]{9}))$",message = "Tel number must look like this example 38096586033")
    @NotBlank
    private String telNumber;
    @Range(min =18,message = "User must be at least 18 years old")
    private  Integer age;
}
