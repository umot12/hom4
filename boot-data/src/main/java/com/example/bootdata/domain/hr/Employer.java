package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name="employee")

public class Employer extends AbstractEntity {


    @Column(name = "employer_name")
   String name;
 String location;


    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "employee_customer",
            joinColumns = { @JoinColumn(name = "employee_id") },
            inverseJoinColumns = { @JoinColumn(name = "customer_id") })

    List<Customer> customers  = new ArrayList<>();

}
