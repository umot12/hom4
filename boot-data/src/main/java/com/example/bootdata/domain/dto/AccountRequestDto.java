package com.example.bootdata.domain.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AccountRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    private String number = UUID.randomUUID().toString();

    private  String  currency;
    @Range(min = -9223372036854775808l)
    private Double balance = (double) 0;
}
