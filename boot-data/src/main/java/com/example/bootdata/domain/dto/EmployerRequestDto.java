package com.example.bootdata.domain.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class EmployerRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    @NotNull
    @Size(min = 3, message = "company name should have at least 3 characters")
    String name;
    @NotNull
    @Size(min = 3, message = "company location should have at least 3 characters")
    String location;
}
