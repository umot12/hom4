package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")

@Entity
@Table(name="accounts")
public class Account extends AbstractEntity {


    private String number = UUID.randomUUID().toString();

    private  String  currency;

    private Double balance = (double) 0;
    @JsonIgnore
    @ManyToOne(cascade ={CascadeType.MERGE } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Account(String currency) {
        this.currency = currency;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Account{" +
              //  "id=" + id +
                ", number='" + number + '\'' +
                ", currency='" + currency + '\'' +
                ", balance=" + balance +
                '}';
    }



    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}

