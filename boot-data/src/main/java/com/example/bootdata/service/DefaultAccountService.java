package com.example.bootdata.service;


import com.example.bootdata.dao.AccountJpaRepository;
import com.example.bootdata.domain.hr.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultAccountService  implements AccountService {

    private final AccountJpaRepository  accountRepository;
    @Override
    public void save(Account account) {

        account.setNumber(UUID.randomUUID().toString());

        accountRepository.save(account);
    }

    @Override
    public void update(Account account) {
        account.setCreationDate(
                accountRepository .getOne(account.getId()).getCreationDate());
        accountRepository.save(account);
    }

    @Override
    public void delete(Account account) {
        accountRepository.delete(account);
    }

    @Override
    public void deleteAll(List<Account> accountList) {
        accountRepository.deleteAll(accountList );
    }

    @Override
    public void saveAll(List<Account> accountList) {
        accountRepository.saveAll(accountList);
    }
@Transactional(readOnly=true)
    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        accountRepository.deleteById(id);

    }
    @Override
    public List<Account> findAll(Integer  page, Integer size){

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Account> departmentPage = accountRepository.findAll(pageable);

        return departmentPage.toList();

    }
@Transactional

public boolean addMoney( UUID number,  Double  amount ){

    Optional<Account> accountOptional = accountRepository.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny() ;

    if(accountOptional.isEmpty()){
        return  false;
    }
    Account editedAccount = accountOptional.get();

    Double accountBalance =  accountOptional.get().getBalance();

    Double accountNewBalance = accountBalance + amount ;

    editedAccount.setBalance(accountNewBalance);

    accountRepository.save( editedAccount);

return true;




}
@Override
public boolean takeMoney(UUID number, Double  amount ){
    Optional  <Account> accountOptional = accountRepository.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny();
    if(accountOptional.isEmpty()){
        return false;
    }
    Account editedAccount = accountOptional.get();

    Double accountBalance =  accountOptional.get().getBalance();
    if(accountBalance - amount< 0){
        return false;
    }

    Double accountNewBalance = accountBalance - amount;

    editedAccount.setBalance(accountNewBalance);
    accountRepository.save(editedAccount);

    return true;


}

    @Transactional(readOnly=true)
    @Override
    public Account getOne(long id) {
        return accountRepository.getOne(id);
    }


}
