package com.example.bootdata.service;

import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;

import java.util.List;
import java.util.UUID;

public interface CustomerService {
    List<Customer> findAll(Integer page, Integer size);
    void save(Customer customer );
    void update(Customer customer);
    void delete(Customer customer);
    void deleteAll(List<Customer> customers);
    void saveAll(List<Customer> customers);
    List<Customer> findAll();
     void  deleteById(Long id);
    Customer getOne(Long id);

    public boolean addAccount(Long userId, Account account);


}
