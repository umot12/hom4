package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.CustomerDto;

import com.example.bootdata.domain.hr.Customer;

import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapper extends DtoMapperFacade<Customer, CustomerDto>{
    public CustomerDtoMapper (){super(Customer.class , CustomerDto.class); }
}
