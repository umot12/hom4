package com.example.bootdata.service;



import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.UserDto;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserDtoMapper extends DtoMapperFacade<SysUser, UserDto> {
    public UserDtoMapper() {
        super(SysUser.class, UserDto.class);
    }

    @Override
    protected void decorateDto(UserDto dto, SysUser entity) {
        String roles = entity.getSysRoles().stream().map(SysRole::getRoleName).collect(Collectors.joining(", "));
        dto.setSysRoles(roles);
    }
}
