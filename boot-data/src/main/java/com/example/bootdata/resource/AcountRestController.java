package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.AccountDto;
import com.example.bootdata.domain.dto.AccountRequestDto;
import com.example.bootdata.domain.hr.Account;

import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.service.AccountService;

import com.example.bootdata.service.CustomerService;
import com.example.bootdata.service.dtomapper.AccountDtoMapper;
import com.example.bootdata.service.dtomapper.AccountRequestDtoMapper;
import com.example.bootdata.service.dtomapper.CustomerDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/accounts")
@CrossOrigin(origins = {"http://localhost:3000"})
public class AcountRestController {

    private final AccountService accountService;

    private final AccountDtoMapper dtoMapper;
    private final CustomerDtoMapper customerMapper;

    private final AccountRequestDtoMapper requestDtoMapper;
    @GetMapping

    public List<AccountDto> findAll(){
        return accountService.findAll(0,10).stream()
                .map(dtoMapper::convertToDto)
                .collect(Collectors.toList());

    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Account> departments = accountService.findAll(page, size);
        List<AccountDto> departmentsDto = departments.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(departmentsDto);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long studentId){
   Account account = accountService.getOne(studentId);
        if (account  == null){
            return ResponseEntity.badRequest().body("Student not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(account) );
    }
    @GetMapping("/{id}/customers")
    public ResponseEntity<?> getCustomer(@PathVariable ("id") Long studentId){
        Account account = accountService.getOne(studentId);
        if (account  == null){
            return ResponseEntity.badRequest().body("Student not found");
        }
        return ResponseEntity.ok().body(customerMapper.convertToDto(account.getCustomer()) );
    }


    @PostMapping
    public void create(@RequestBody AccountRequestDto account ){
//Account newAccount = ;
       // newAccount.setNumber(UUID.randomUUID().toString());
        accountService.save(requestDtoMapper.convertToEntity( account));
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody AccountRequestDto accountRequestDto ){
        try {


            accountService.update(requestDtoMapper.convertToEntity(accountRequestDto)) ;
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping

    public ResponseEntity <?> deleteAccount(@RequestBody AccountRequestDto  account){

        try{

            accountService.delete(requestDtoMapper.convertToEntity(account));
            return    ResponseEntity.ok().build();

        }catch (RuntimeException e){

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable ("id") Long studentId){
        try {
            accountService.deleteById(studentId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @PutMapping("/balance")
    public ResponseEntity <?>  addMoney(@RequestParam UUID number, @RequestParam Double  amount ){



        if(accountService.addMoney(number,amount) == false){
            return ResponseEntity.badRequest().body("Account not found");
        }


        return ResponseEntity.ok().build();

    }

    @PutMapping("/balance/{id}/{amount}")
    public ResponseEntity <?>  replenishAccount(@PathVariable ("id") Long accountId,@PathVariable ("amount") Double amount){


        Account editedAccount = accountService.getOne(accountId);

        Double accountBalance =  accountService.getOne(accountId).getBalance();

        Double accountNewBalance = accountBalance + amount ;

        editedAccount.setBalance(accountNewBalance);

        accountService.update( editedAccount);


        return ResponseEntity.ok().build();

    }


    @DeleteMapping("/balance")
    public ResponseEntity <?>  takeMoney(  @RequestParam  UUID  number, @RequestParam Double  amount ){


        if(accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny().isPresent()){
            if(accountService.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny().get().getBalance() -amount <0){
                return ResponseEntity.badRequest().body("There is not enough money on your account");
            }
        }
if(accountService.takeMoney(number,amount) == false){
    return ResponseEntity.badRequest().body("Account not found");}else{
    return ResponseEntity.ok().build();
}
}


    @PutMapping("/balance/transaction")

    public ResponseEntity <?>  transactionMoney(  @RequestParam  UUID  numberAccountFrom, @RequestParam  UUID  numberAccountTo, @RequestParam Double  amount ){
        Optional  <Account> accountFromOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountFrom.toString())).findAny();
        if(accountFromOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        Optional  <Account> accountToOptional = accountService.findAll().stream().filter(el->el.getNumber().equals(numberAccountTo.toString())).findAny();
        if(accountToOptional.isEmpty()){
            return ResponseEntity.badRequest().body("Account not found");
        }
        if(accountFromOptional.get().getBalance() -amount <0){

            return ResponseEntity.badRequest().body("Not enough money on account");
        }
        takeMoney(numberAccountFrom ,amount);
        addMoney(numberAccountTo,amount) ;

        return ResponseEntity.ok().build();

    }


}
