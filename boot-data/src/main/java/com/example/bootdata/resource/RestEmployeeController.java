package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.EmployerDto;
import com.example.bootdata.domain.dto.EmployerRequestDto;
import com.example.bootdata.domain.hr.Employer;


import com.example.bootdata.service.EmployerService;
import com.example.bootdata.service.dtomapper.EmployerDtoMapper;
import com.example.bootdata.service.dtomapper.EmployerRequestDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employees")
public class RestEmployeeController {
    private final EmployerService employeeService;

    private final EmployerDtoMapper dtoMapper;
    private final EmployerRequestDtoMapper requestMapper;

    @GetMapping
    public List<EmployerDto > getAll() {
        return employeeService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Employer> departments =employeeService.findAll(page, size);
        List<EmployerDto> departmentsDto = departments.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(departmentsDto);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?>  getById(@PathVariable("id")  Long  userId) {
       Employer  employee = employeeService.getById(userId );

        if (employee   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(employee ) );
    }
    @GetMapping("/customers/{id}")
    public ResponseEntity<?>  getCustomers(@PathVariable("id")  Long  userId) {
        Employer  employee = employeeService.getById(userId );

        if (employee   == null){
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(employee.getCustomers() );
    }
    @PostMapping
    public void create(@RequestBody EmployerRequestDto employee ){
        employeeService.create(requestMapper.convertToEntity(employee) );
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id")Long userId) {
        try {
          employeeService.deleteById(userId);
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping
    public ResponseEntity<?> deleteEmployee(@RequestBody EmployerRequestDto  company) {
        try {
            employeeService.delete(requestMapper.convertToEntity(company));
            return ResponseEntity.ok().build();
        }catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping
    public ResponseEntity <?> update(@RequestBody EmployerRequestDto employee) {
        try {
System.out.println(employee);
//Employer employer =requestMapper.convertToEntity(employee);
//employer.setCreationDate(employeeService.getById(employer.getId()).getCreationDate());

            employeeService.update(requestMapper.convertToEntity(employee));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }


}
