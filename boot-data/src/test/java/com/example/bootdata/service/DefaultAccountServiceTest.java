package com.example.bootdata.service;

import com.example.bootdata.dao.AccountJpaRepository;
import com.example.bootdata.dao.CustomerJpaRepository;

import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.service.DefaultAccountService;
import com.example.bootdata.service.DefaultCustomerService;
import com.example.bootdata.service.DefaultEmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultAccountServiceTest {

    @Mock
    private AccountJpaRepository accountJpaRepository;


    @InjectMocks
    private DefaultAccountService accountService;

    @Captor
    private ArgumentCaptor<Account> accountArgumentCaptor;

    @Test
    public void testGetAllPageble() {
        Account account = new Account();
        when(accountJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(account)));
        List<Account> accounts = accountService.findAll(1, 2);

        assertEquals(account, accounts.get(0));
    }

    @Test
    public void test_GetAll_Success() {
        Account account1 = new Account();
        Account account2 = new Account();
        List<Account> accountsExpected = List.of(account1, account2);
        when(accountJpaRepository.findAll())
                .thenReturn(  accountsExpected);

        List<Account> accountsActual = accountService.findAll();
        assertNotNull(accountsActual);
        assertFalse(accountsActual.isEmpty());
        assertIterableEquals(accountsExpected, accountsActual);
    }

    @Test
    public void test_Create_Success() {
       Account  account1 = new Account() ;

        accountService.save(account1);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        Account  accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_Put_Success() {
        Account  account1 = new Account() ;


        account1.setBalance(345.0);
        when(accountJpaRepository.getOne(account1.getId())
        )
                .thenReturn(account1);

        accountService.update(account1);

        verify(accountJpaRepository).save(accountArgumentCaptor.capture());
        Account  accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        Account  account1 = new Account() ;

        accountService.save(account1);
        account1.setId(1L);

        account1.setBalance(345.0);
        accountService.delete(account1);

        verify(accountJpaRepository).delete(accountArgumentCaptor.capture());
        Account  accountActualArgument = accountArgumentCaptor.getValue();
        assertEquals(account1, accountActualArgument);
    }
    @Test
    public void test_GetById_Success() {
       Account account1 = new Account();
        account1.setId(1L);
        Account  account2 = new Account();
        account2.setId(2L);
        Account   accountExpected = account2;
        when(accountJpaRepository.getOne(account2.getId()))
                .thenReturn(account2);

        Account accountActual =accountService.getOne(account2.getId());
        assertNotNull(accountActual);

        assertEquals(accountExpected, accountActual);
    }
}