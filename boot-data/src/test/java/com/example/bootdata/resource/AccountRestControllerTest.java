package com.example.bootdata.resource;

import com.example.bootdata.dao.AccountJpaRepository;
import com.example.bootdata.dao.CustomerJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.AccountDto;
import com.example.bootdata.domain.dto.AccountRequestDto;
import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.repository.UserRepository;
import com.example.bootdata.resource.CustomerRestController;
import com.example.bootdata.service.*;
import com.example.bootdata.service.dtomapper.*;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.test.context.support.WithMockUser;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AcountRestController.class)

public class AccountRestControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private CustomerJpaRepository customerJpaRepository;
@MockBean
private AccountJpaRepository accountJpaRepository;
    @MockBean

    private UserRepository userRepository ;
    @MockBean
    private CustomerService customerService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private CustomOAuth2UserService oauthUserService;
    @MockBean
    private AccountService accountService;
    @MockBean
    private CustomerDtoMapper dtoMapper;
    @MockBean
    private CustomerRequestDtoMapper requestMapper;
    @MockBean
    private AccountDtoMapper accountMapper;
    @MockBean
    private EmployerDtoMapper employerMapper;
    @MockBean
    private AccountRequestDtoMapper requestAccountMapper;
    @TestConfiguration
    public static class TestConfig {
        @Bean // Тестируем компонентно с меппером
        public UserDtoMapper employeeDtoMapper() {
            return new UserDtoMapper();
        }
    }
    @BeforeEach
    public void setUp()  throws Exception  {
        SysUser user = new SysUser(1L, "1", "1", true, Set.of(new SysRole(1L, "USER", null)));
        when(userDetailsService.findAll()).thenReturn(List.of(user));

    }
    @Test
    @WithMockUser(value = "a")

    public void getById() throws Exception {
        Account account = new Account();
        account.setId(1L);
        account.setCurrency("GBP");


        AccountDto accountDto = new AccountDto();
        accountDto.setId(1L);
       accountDto.setCurrency("GBP");


        when(accountService.getOne(1L)).thenReturn(account);
        when(accountMapper.convertToDto(account)).thenReturn(accountDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/accounts/1").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.currency", Matchers.is("GBP")));
    }

    @Test
    @WithMockUser(value = "user1")
    public void findAll() throws Exception {
        Account account = new Account();
        account.setId(1L);
        account.setCurrency("GBP");


        AccountDto accountDto = new AccountDto();
        accountDto.setId(1L);
        accountDto.setCurrency("GBP");
List<Account> accounts = new ArrayList<>();
accounts.add(account);

        when(accountService.findAll(0,10)).thenReturn(accounts);
        when(accountMapper.convertToDto(account)).thenReturn(accountDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/accounts").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].currency", Matchers.is("GBP")));
    }
    @Test
    @WithMockUser(value = "user1")
    public void findAllPageable() throws Exception {
        Account account = new Account();
        account.setId(1L);
        account.setCurrency("GBP");


        AccountDto accountDto = new AccountDto();
        accountDto.setId(1L);
        accountDto.setCurrency("GBP");
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        int page = 0;
        int size =10;

        when(accountService.findAll(page,size)).thenReturn(accounts);
        when(accountMapper.convertToDto(account)).thenReturn(accountDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/accounts/0/10").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].currency", Matchers.is("GBP")));
    }

    @Test
    @WithMockUser(value = "user1")
    public void testCreate() throws Exception {


        this.mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                   "id": 10,
                                   "number": "",
                                   "currency": "string",
                                   "balance": 0
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")


    public void testPut() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.put("/accounts")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                   "id": 114,
                                   "number": "",
                                   "currency": "string",
                                   "balance": 123
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDelete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/accounts")
                        .contentType("application/json")
                        .content(
                                """
                                
                                   {
                                               "id": 120,
                                               "number": "",
                                               "currency": "",
                                               "balance": 0
                                             }
                                   
                                
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDeleteById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/accounts/116").contentType("application/json"))
                .andExpect(status().isOk())


        ;
    }


}