package com.example.bootdata.resource;

import com.example.bootdata.dao.AccountJpaRepository;
import com.example.bootdata.dao.CustomerJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.AccountDto;
import com.example.bootdata.domain.dto.CustomerDto;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.resource.CustomerRestController;
import com.example.bootdata.service.*;
import com.example.bootdata.service.dtomapper.*;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerRestController.class)

public class CustomerRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomOAuth2UserService oauthUserService;

    @MockBean
    private CustomerJpaRepository customerJpaRepository;
    @MockBean
    private  CustomerService customerService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;
    @MockBean
    private AccountJpaRepository accountJpaRepository;

    @MockBean
    private  AccountService accountService ;
    @MockBean
    private  CustomerDtoMapper dtoMapper;
    @MockBean
    private  CustomerRequestDtoMapper requestMapper;
   @MockBean
    private   AccountDtoMapper accountMapper;
   @MockBean
    private   EmployerDtoMapper employerMapper;
    @MockBean
    private  AccountRequestDtoMapper requestAccountMapper;
    @TestConfiguration
    public static class TestConfig {
        @Bean // Тестируем компонентно с меппером
        public UserDtoMapper employeeDtoMapper() {
            return new UserDtoMapper();
        }
    }
    @BeforeEach
    public void setUp()  throws Exception  {
        SysUser user = new SysUser(1L, "1", "1", true, Set.of(new SysRole(1L, "USER", null)));
        when(userDetailsService.findAll()).thenReturn(List.of(user));

    }
    @Test

    @WithMockUser(value = "user1")
    public void getById() throws Exception {
        Customer customer = new Customer("Kris","kris@gmail.com",23);
        customer.setId(1L);


        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setName("Kris");

        customer.setName("Kris");
        when(customerService.getOne(1L)).thenReturn(customer);
        when(dtoMapper.convertToDto(customer)).thenReturn(customerDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers/1").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Kris")));
    }

    @Test
    @WithMockUser(value = "user1")
    public void findAll() throws Exception {
        Customer customer = new Customer();

List <Customer> customers = new ArrayList<>();
customers.add(customer);
        CustomerDto customerDto = new CustomerDto();
       customerDto.setId(10L);

       customerDto.setName("Kris");
        when(customerService.findAll(0,10)).thenReturn(customers);
        when(dtoMapper.convertToDto(customers.get(0))).thenReturn(customerDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Kris")));
    }
    @Test
    @WithMockUser(value = "user1")
    public void findAllPageable() throws Exception {
        Customer customer = new Customer();

        List <Customer> customers = new ArrayList<>();
        customers.add(customer);
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(10L);

        customerDto.setName("Kris");
        int page = 0;
        int size = 10;
        when(customerService.findAll(page,size)).thenReturn(customers);
        when(dtoMapper.convertToDto(customers.get(0))).thenReturn(customerDto);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/customers/0/10").contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("Kris")));
    }
    @Test
    @WithMockUser(value = "user1")
    public void testCreate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/customers")
                        .contentType("application/json")
                        .content(
                                """
                                {
                                    "id": 11,
                                    "name": "Jane",
                                    "password": 123,
                                    "email": "email@gmail.com",
                                    "telNumber": "380325484938",
                                    "age": 19
                                   
                                }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")
    public void testPut() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/customers")
                .contentType("application/json")
                .content(
                        """
                       {
                           "id": 106,
                           "name": "Jane",
                           "email": "stringJane@ukr.net",
                           "password": "string",
                           "telNumber": "380325484938",
                           "age": 23
                         }
                        """
                ));
        this.mockMvc.perform(MockMvcRequestBuilders.put("/customers")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                   "id": 106,
                                   "name": "Jane",
                                   "email": "stringJane@ukr.net",
                                   "password": "string",
                                   "telNumber": "380325484938",
                                   "age": 23
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }

    @Test
    @WithMockUser(value = "user1")
    public void testDelete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/customers")
                        .contentType("application/json")
                        .content(
                                """
                                {
                                    "id": 103,
                                    "name": "Jane",
                                    "password": 123,
                                    "email": "email@gmail.com",
                                    "telNumber": "+38093100111",
                                    "age": 19
                                   
                                }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDeleteById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/customers/110").contentType("application/json"))
                .andExpect(status().isOk())


        ;
    }
}